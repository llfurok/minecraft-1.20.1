# ГОСПОДА ПРИСЕДАЮЩИЕ

### Для игры на сервере необходимо:

- [Установить TLauncher (кликабельно)](https://tlauncher.org/)
- [Установить Модпак (кликабельно)](https://gitlab.com/llfurok/minecraft-1.20.1/-/archive/main/minecraft-1.20.1-main.zip) - распаковать, запустить automatic.exe
- Непосредственно в самой игре добавить сервер 81.88.119.25

<details>
<summary>Список модов (Клиент+Сервер):</summary>

| Название | Описание |
| ------ | ------ |
|Aether Legacy|Рай!|
|Aquatic Frontiers|Водяные существа|
|Breedable Killer Rabbit|Кролики|
|Caracal Mod|Добавляет каракАла|
|Doggy Talents Next|Уровни и навыки для ПЁСЕЛЕЙ|
|Easier Sleeping|Показывает кто спал и сколько %|
|Enhanced AI|Враждебные мобы ебут жоска|
|Falling Tree|Быстрый сруб дерева|
|Friendly Fire|Убирает урон по питомцам|
|Giant Spawn|С вероятностью (настраиваемой) появляется гигантский|
|Reap Mod|Собирать урожай (ПКМ)|
|Iron Chest|Сундуки|
|Sit|Сидеть на лестнице / плитке (ПКМ)|

</details>

<details>
<summary>Список модов (Клиент):</summary>

| Название | Описание |
| ------ | ------ |
|Better Animations|Животные чуть интересней|
|Better FPS|Ускорение фпс засчет кругового рендера|
|Dark Mode Everywhere|Темная тема|
|Distant Friends|Фантом "игрока", следящего за вами|
|Enchantment Descriptions|Добавляет описание зачарам|
|JEI|Рецепты и вроде бы F7 освещение|
|Physics Mod|Разрушаемость, смерть монстров|
|Optifine|Светит факел в руке|
|ReBlured|Размытие в инвентаре (фокус)|
|Xaero's Minimap|Карта (! без пещер и мобов !)|

</details>

<details>
<summary>Используемые библиотеки:</summary>

| Название |
| ------ |
|Architectury API|
|Balm|
|Bookshelf|
|Creative Core|
|Cloth Config|
|Collective|
|Fabric API|
|Gecko Lib|
|Insane Lib|
|Puzzles Lib|

</details>